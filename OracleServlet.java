import java.util.*;
import java.sql.*;
import oracle.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;



public class OracleServlet extends HttpServlet { 

   public void doGet(HttpServletRequest req, HttpServletResponse resp) 
      throws ServletException, IOException {
      
      // this handles the initial request and should prompt the user
      // for a username and password
      
      resp.setContentType("text/html");
      PrintWriter out = resp.getWriter();
      
      out.println("<html><head><title>Oracle Servlet, Password Request</title></head>");
      out.println("<body><p>Please enter your username and password</p>");
      out.println("<form method=\"post\" action=\"/servlets/Oracle\">");
      out.println("<input type=\"text\" name=\"username\" size=\"20\"><br>");
      out.println("<input type=\"password\" name=\"password\" size=\"20\"><br>");
      out.println("<input type=\"submit\" value=\"Submit Query\"></form></body></html>");

   }
   
   
   public void doPost(HttpServletRequest req, HttpServletResponse resp) 
      throws ServletException, IOException {
      
      resp.setContentType("text/html");
      PrintWriter out = resp.getWriter();

      out.println("<html><head><title>Oracle Servlet, Results</title></head>");
      
      try {
      
         DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
         Connection con = DriverManager.getConnection("jdbc:oracle:thin:@badboy:1521:t2",req.getParameter("username"), req.getParameter("password"));
         
         Statement smnt = con.createStatement();
         String sqlquery = new String("SELECT e_desc,e_date,e_attendees,e_id,e_ltxt FROM events");
         ResultSet res = smnt.executeQuery( sqlquery );
         
         out.println("<body><p>Here are your results</p><table border=\"1\" cellspacing=\"0\"><tr>");
         out.println("<td><b>Description</b></td><td><b>Date</b></td><td><b>Atts</b></td><td><b>ID</b></td></tr><tr>");
         
         while (res.next()) {
            
            out.println("<td>" + res.getString("e_desc") + "</td>");
            out.println("<td>" + res.getString("e_date") + "</td>");
            out.println("<td>" + res.getString("e_attendees") + "</td>");
            out.println("<td>" + res.getString("e_id") + "</td></tr>");
            out.println("<tr><td colspan=\"4\">" + res.getString("e_ltxt") + "</td></tr>");
         }
         
         out.println("</tr></table></body></html>");
         
         res.close();
         smnt.close();
         con.close();
         
      }
      
      catch (SQLException sqlex) {
      
         String msg = new String(sqlex.getMessage());
         
         out.println("<body><p>Something broke...<br>");
         out.println("SQL Exception:" + msg + "</p></body></html>");

      
      }

   }

}


