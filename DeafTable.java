import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;
import java.util.*;


public class DeafTable extends JFrame {

	public static void main(String[] a) {
		DeafTable dt = new DeafTable();
		dt.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	DeafTable() {
		JDesktopPane jdp = new JDesktopPane();
		Container cp = getContentPane();
		Vector rowA = new Vector(3);
		Vector rowB = new Vector(3);
		Vector rowC = new Vector(3);
		Vector allRows = new Vector(3);
		Vector headings = new Vector(3);
		
		
		headings.add("Qty");
		headings.add("Colour");
		headings.add("Shape");
		
		rowA.add("One");
		rowA.add("Red");
		rowA.add("Circle");

		rowB.add("Two");
		rowB.add("Orange");
		rowB.add("Eye");

		rowC.add("Three");
		rowC.add("Yellow");
		rowC.add("Triangle");
		
		allRows.add(rowA);
		allRows.add(rowB);
		allRows.add(rowC);
		
		JTable t = new JTable(allRows, headings);
		deafenComponent(t);

		Component[] c1 = t.getComponents();
		deafenComponent(c1[0]);



/*		Component[] tChildren = t.getComponents();
		Component[] tCRPaneKids = ((CellRendererPane)tChildren[0]).getComponents();
		
		for (int i=0; i < tCRPaneKids.length; i++ ) {
			deafenComponent(tCRPaneKids[i]);
		}

/*
			public boolean isCellEditable(int row, int col){
				return false;
			}
		};
		t.setRowSelectionAllowed(false);
		t.setColumnSelectionAllowed(false);
*/
		
		cp.add(new JScrollPane(t));
		cp.add(new JButton("click"), BorderLayout.SOUTH);
		pack();
		show();
	}


	private void deafenComponent(Component c) {
System.out.println(c);
		MouseListener[] existingEars = c.getMouseListeners();
		for (int i=0; i < existingEars.length; i++ ) {
			c.removeMouseListener(existingEars[i]);
		}
		MouseMotionListener[] existingMotionEars = c.getMouseMotionListeners();
		for (int i=0; i < existingMotionEars.length; i++ ) {
			c.removeMouseMotionListener(existingMotionEars[i]);
		}
		MouseWheelListener[] mouseWheelEars = c.getMouseWheelListeners();
		for (int i=0; i < mouseWheelEars.length; i++ ) {
			c.removeMouseWheelListener(mouseWheelEars[i]);
		}
	}
}