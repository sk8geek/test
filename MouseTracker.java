import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseMotionAdapter;
import javax.swing.*;

public class MouseTracker extends JFrame {

	private JLabel mx = new JLabel("xx");
	private JLabel mon = new JLabel("ukn");
	private JLabel my = new JLabel("yy");
	
	MouseTracker(){
		setTitle("MouseTracker");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		setSize(200,200);
		
		addMouseListener(new MouseAdapter(){
			public void mouseEntered(MouseEvent evnt){
				mon.setText("Mouse at home");
			}
			public void mouseExited(MouseEvent evnt){
				mon.setText("Oh, sad, mouse has gone.");
			}
		});
		
		addMouseMotionListener(new MouseMotionAdapter(){
			public void mouseMoved(MouseEvent evnt){
				mx.setText(Integer.toString(evnt.getX()));
				my.setText(Integer.toString(evnt.getY()));
			}
		});
		
		Container cp = getContentPane();
		cp.add(mx, BorderLayout.NORTH);
		cp.add(mon);
		cp.add(my, BorderLayout.SOUTH);
		show();
	}
	
	public static void main(String[] a){
		MouseTracker t = new MouseTracker();
	}

}