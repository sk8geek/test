import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.applet.*;


public class AppletSpacebarTest extends JApplet {


	public void init() {
		userInterface();
	}

	private void userInterface () {
		final Container cp = getContentPane();
		JButton setRed = new JButton("Red");
		JButton setGreen = new JButton("Green");
		JButton setBlue = new JButton("Blue");
		final JPanel buttons = new JPanel();
		setRed.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evnt){
				buttons.setBackground(Color.RED);
			}
		});
		setGreen.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evnt){
				buttons.setBackground(Color.GREEN);
			}
		});
		setBlue.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evnt){
				buttons.setBackground(Color.BLUE);
			}
		});
		buttons.add(setRed);
		buttons.add(setGreen);
		buttons.add(setBlue);
		cp.add(buttons);
	}
}