import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;


public class DeafCheckBox2 extends JFrame {

	public static void main(String[] a) {
		DeafCheckBox2 youWhat = new DeafCheckBox2();
		youWhat.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	DeafCheckBox2() {
		Container cp = getContentPane();

		JButton btn = new JButton("nothing");
		cp.add(btn, BorderLayout.NORTH);
		
		JCheckBox cbx = new JCheckBox("Can't click");
		JCheckBox cby = new JCheckBox("Can click");
		
		MouseListener[] ears = cbx.getMouseListeners();
		cbx.removeMouseListener((MouseListener)ears[0]);
		cbx.setFocusable(false);
		cbx.setSelected(true);

		cp.add(cbx);
		cp.add(cby, BorderLayout.SOUTH);


		pack();
		show();
	}
}