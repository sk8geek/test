import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class GBLTest extends JFrame {

	public static void main(String[] a){
		GBLTest frame = new GBLTest();
	}
	
	GBLTest() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("GridBagLayout");
		Container cp = getContentPane();
		cp.setLayout(new GridBagLayout());
		
		GridBagConstraints gbc = new GridBagConstraints();
		// change settings as desired
		gbc.insets = new Insets(8, 16, 8, 16);
		// add components
		addItem(new JLabel("Label"), 0, 0, 1, 1, 
			GridBagConstraints.WEST, cp, gbc);
		addItem(new JTextField(8), 0, 1, 1, 1, 
			GridBagConstraints.WEST, cp, gbc);
		pack();
		setVisible(true);
	}
	
	// this method just makes it easier to add items to the layout
	private static void addItem(Component c, int x, int y, int w, int h, int a, 
		Container gfpContentPane, GridBagConstraints gbc) {
		gbc.gridx = x;
		gbc.gridy = y;
		gbc.gridwidth = w;
		gbc.gridheight = h;
		gbc.anchor = a;
		gfpContentPane.add(c, gbc);
	}

}