import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class JDesktopPaneTest2 extends JFrame {

	public static void main(String[] a){
		JDesktopPaneTest2 s = new JDesktopPaneTest2();
	}
	
	JDesktopPaneTest2() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("JDesktopPaneTest2");
		JDesktopPane dtp = new JDesktopPane();
		setLayeredPane(dtp);
		setSize(new Dimension(640,480));
		setVisible(true);
	}
}