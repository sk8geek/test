import java.applet.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.net.*;

public class ChangeAppletTest extends JApplet {

	private Image displayedImage;
	private JPanel imgSpace;
	private int currentImg = 0;
	private Image[] imgs = new Image[2];

	public void init() {
		try {
			URL src = new URL("http://bronze/test/images/");
			imgs[0] = getImage(src, "testimg1.png");
			imgs[1] = getImage(src, "testimg2.png");
		} catch (MalformedURLException malex) {}
		Button b_nextImg = new Button("Next");
		b_nextImg.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				nextImg();
			}
		});
		imgSpace = new JPanel(){
			protected void paintComponent(Graphics g) {
				super.paintComponent(g);
				g.drawImage(displayedImage, 0, 0, this);
			}
		};

		imgSpace.setSize(250,300);
		Container cp = getContentPane();
		cp.setLayout(new BorderLayout());
		cp.add(imgSpace);
		cp.add(b_nextImg, BorderLayout.SOUTH);
	}

	public void start() {
		displayedImage = imgs[currentImg];
	}
	
	private void nextImg() {
		currentImg++;
		if (currentImg > 1) { currentImg = 0; }
		displayedImage = imgs[currentImg];
		repaint();
	}
	
	public void stop() {}
	
	public void destroy() {}
	
}