import java.util.*;
import java.awt.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;

public class HighlightedChangesTable extends JTable implements ChangeListener {

	HighlightedChangesTable(Vector rowData, Vector colHeadings) {
		super(rowData, colHeadings);
	}
	
	public void stateChanged(ChangeEvent evnt) {
		System.out.println(evnt);
	}

}