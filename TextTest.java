
class TextTest {

	public static void main (String[] args) {
		if (textOnly(args[0])) {
			System.out.println("text only");
		}
	}
	
	private static boolean textOnly(String testMe) {
		for (int c = 0; c < testMe.length(); c++) {
			if ( !Character.isLetterOrDigit(testMe.charAt(c))) {
				System.out.println(testMe.charAt(c));
				return false;
			}
		}
		return true;
	}

}