import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class TransButton extends JFrame {

	public static void main(String[] a){
		TransButton cleanFrame = new TransButton();
	}
	
	TransButton() {
		// user interface
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("TransButton");
		Container cp = getContentPane();
		cp.setBackground(new Color(0, 0, 255));
		cp.setLayout(new FlowLayout(FlowLayout.CENTER, 40, 40));
		
		JButton button = new JButton("Press Me");
		button.setBackground(new Color(255, 0, 0, 127));
//		button.setContentAreaFilled(false);
//		button.setRolloverEnabled(false);
//		button.setOpaque(false);
		button.setFocusPainted(false);
		
		cp.add(button);
		pack();
		setVisible(true);
	}
}