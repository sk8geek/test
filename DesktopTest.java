import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class DesktopTest extends JInternalFrame {

	private JDesktopPane dtp;

	DesktopTest() {
    setTitle("J Internal Frame");
    setDefaultCloseOperation(JInternalFrame.EXIT_ON_CLOSE);
		dtp = getDesktopPane();
		userInterface();
	}
	
	private void userInterface() {
		JButton b_test = new JButton("Test");
    Container cp = getContentPane();
    cp.add(b_test);
    pack();
    show();
  }
	
	
	public static void main (String[] args) {
		DesktopTest t = new DesktopTest();
  }

}