import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;

public class StockCutter extends JFrame{

	private JTextField stockLength = new JTextField("10", 4);
	private JTextField firstLength = new JTextField("", 4);
	private JTextField secondLength = new JTextField("", 4);
	private JTextField thirdLength = new JTextField("", 4);
	private JTextField firstReq = new JTextField("", 4);
	private JTextField secondReq = new JTextField("", 4);
	private JTextField thirdReq = new JTextField("", 4);
	private	int x = 0;
	private	int y = 0;
	private	int z = 0;
	private int s = 0;
	private int A = 0;
	private int B = 0;
	private int C = 0;
	private int W = 0;
	private ArrayList combos = new ArrayList();
	private Object[] slices;
	private JTextField wasteLength = new JTextField("0", 4);
	private JTextField optimum = new JTextField("", 4);
	private	JButton calculate = new JButton("Calculate");

	public static void main(String args[]){
		StockCutter s = new StockCutter();
	}

	StockCutter() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Stock Cutter");
		userInterface();
	}
	
	private void calcPossibleCutArray() {
		combos.clear();
		int s = Integer.parseInt(stockLength.getText());
		int a = 0;
		int b = 0;
		int c = 0;
		while (fits(a, b, c) >= 0) {
			if (fits(a, b, c) >= 0) {
//				add(a, b, c);
			}
			while (fits(a, b, c) >= 0) {
				if (fits(a, b, c) >= 0) {
//					add(a, b, c);
				}
				while (fits(a, b, c) >= 0) {
					if (fits(a, b, c) >= 0) {
						add(a, b, c);
					}
					c++;
				}
				c = 0;
				b++;
			}
			b = 0;
			a++;
		}
		combos.trimToSize();
		slices = combos.toArray();
		Arrays.sort(slices);
		for (int i = 0; i < slices.length; i++) {
			System.out.println(((cutCombo)slices[i]).view());
		}
	}
	
	private void add(int a, int b, int c) {
		int w = fits(a, b, c);
		combos.add(new cutCombo(a, b, c, w));
	}
	
	private int fits(int a, int b, int c) {
		return (s - (a * x + b * y + c * z));
	}
	
	private void calculatePossibleCuts() {
		x = Integer.parseInt(firstLength.getText());
		y = Integer.parseInt(secondLength.getText());
		z = Integer.parseInt(thirdLength.getText());
		s = Integer.parseInt(stockLength.getText());
		A = Integer.parseInt(firstReq.getText());
		B = Integer.parseInt(secondReq.getText());
		C = Integer.parseInt(thirdReq.getText());
		int totalLength = (A * x + B * y + C * z);
		double stockCount = (double)totalLength / (double)s;
		if (stockCount != (int)stockCount) {
			stockCount ++;
		}
		optimum.setText(Integer.toString((int)stockCount));
		calcPossibleCutArray();
	}

	private void calculateActualCuts() {
		W = 0;
		int possCutPointer = 0;
		while ( A + B + C > 0 ) {
			if ( A >= B ) {
				possCutPointer += getMoreA(possCutPointer);
				if ( B >= C ) {
					possCutPointer += getMoreB(possCutPointer);
				}
			}
			possCutPointer = validateCut(possCutPointer);
			performCut(possCutPointer);
		}
	}
	
	private int getMoreA(int pointer) {
		int correction = 1;
		if ( pointer
	}
	
	private int getMoreB(int pointer) {
		int correction = 0;
	}
	
	private int validateCut(int pointer) {
	}
	
	private void performCut(int pointer) {
		A -= ((cutCombo)combos.get(pointer)).aCount;
		B -= ((cutCombo)combos.get(pointer)).bCount;
		C -= ((cutCombo)combos.get(pointer)).cCount;
		W += ((cutCombo)combos.get(pointer)).waste;
	}
	
	private void userInterface() {
		wasteLength.setEditable(false);
		optimum.setEditable(false);
	
		JPanel p_labels = new JPanel(new GridLayout(5, 1, 4, 4));
		p_labels.add(new JLabel("Stock length"));
		p_labels.add(new JLabel("First length"));
		p_labels.add(new JLabel("Secnd length"));
		p_labels.add(new JLabel("Third length"));
		p_labels.add(new JLabel("Remainder"));

		JPanel p_fields = new JPanel(new GridLayout(5, 2, 4, 4));
		p_fields.add(stockLength);
		p_fields.add(new JLabel());
		p_fields.add(firstLength);
		p_fields.add(firstReq);
		p_fields.add(secondLength);
		p_fields.add(secondReq);
		p_fields.add(thirdLength);
		p_fields.add(thirdReq);
		p_fields.add(wasteLength);
		p_fields.add(optimum);

		JPanel p_main = new JPanel(new GridLayout(1, 2, 4, 4));
		p_main.add(p_labels);
		p_main.add(p_fields);
		getContentPane().add(p_main);
		
		calculate.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evnt){
				setFieldEditing(false);
				calculatePossibleCuts();
				calculateActualCuts();
				setFieldEditing(true);
			}
		});
		JPanel buttonPanel = new JPanel();
		buttonPanel.add(calculate);
		getContentPane().add(buttonPanel, BorderLayout.SOUTH);
		
		pack();
		setVisible(true);
	}
	
	private void setFieldEditing(boolean editable) {
		stockLength.setEnabled(editable);
		firstLength.setEnabled(editable);
		secondLength.setEnabled(editable);
		thirdLength.setEnabled(editable);
		calculate.setEnabled(editable);
	}
}



class cutCombo implements Comparable{

	int aCount = 0;
	int bCount = 0;
	int cCount = 0;
	public int waste = 0;
	
	public int compareTo(Object o) {
		cutCombo c = (cutCombo)o;
		if (c.waste == waste) {
			return 0;
		}
		return ((c.waste > waste) ? -1 : 1);
	}
	
	cutCombo() {
	}
	
	String view() {
		return new String(aCount + ", " + bCount + ", " + cCount 
			+ ", " + waste);
	}

	cutCombo(int a, int b, int c, int w) {
		aCount = a;
		bCount = b;
		cCount = c;
		waste = w;
	}
}