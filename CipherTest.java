import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.*;
import java.security.*;
import java.security.spec.*;
import javax.crypto.*;
import javax.crypto.spec.*;

public class CipherTest extends JFrame implements ActionListener {

	private JTextArea message = new JTextArea(12, 40);
	private JPasswordField pw = new JPasswordField(12);
	
	public static void main(String[] args) {
		CipherTest c = new CipherTest();
	}
	
	CipherTest() {
		userInterface();
		
	}
	
	private void userInterface() {
		setTitle("Cipher Test");
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		Container cp = getContentPane();
		JPanel p_pw = new JPanel();
		JButton enc = new JButton("Encrypt");
		JButton dec = new JButton("Decrypt");
		p_pw.add(new JLabel("Password:"));
		p_pw.add(pw);
		p_pw.add(enc);
		p_pw.add(dec);
		cp.add(message);
		cp.add(p_pw, BorderLayout.SOUTH);
		enc.setActionCommand("encrypt");
		enc.addActionListener(this);
		dec.setActionCommand("decrypt");
		dec.addActionListener(this);
		pack();
		setVisible(true);
	}

	private byte[] getCodedContent() {
	    FileInputStream fr;
    	ObjectInputStream ois;
    	byte[] secretInfo;
    	try {
    		fr = new FileInputStream(new File("secret.ser"));
        	ois = new ObjectInputStream(fr);
          	secretInfo = (byte[])ois.readObject();
System.out.println("read " + secretInfo.length); //XXX
            ois.close();
            fr.close();
  	        return secretInfo;
		} catch (FileNotFoundException fnfex) {
        	System.out.println(fnfex);
        } catch (ClassNotFoundException cnfex) {
        	System.out.println(cnfex);
        } catch (EOFException eofex) {
        	System.out.println(eofex);
        } catch (IOException ioex) {
        	System.out.println(ioex);
        }
        return null;
    }

	public void actionPerformed(ActionEvent evnt) {
		String userCmd = evnt.getActionCommand();
		
		try {
			byte[] salt = (new String("chipsvin")).getBytes();
			PBEParameterSpec codeSpec = new PBEParameterSpec(salt, 23);
			PBEKeySpec keySpec = new PBEKeySpec(pw.getPassword());
			SecretKeyFactory keyFac = 
				SecretKeyFactory.getInstance("PBEWithMD5AndDES");
			SecretKey key = keyFac.generateSecret(keySpec);
			Cipher c = Cipher.getInstance("PBEWithMD5AndDES");
			if (userCmd == "encrypt") {
				c.init(Cipher.ENCRYPT_MODE, key, codeSpec);
				byte[] saveMessage = c.doFinal(message.getText().getBytes());
//			String saveMessage = message.getText();
      			ObjectOutputStream serOut = new ObjectOutputStream(
	        		new FileOutputStream(new File("secret.ser")));
System.out.println("wrote " + saveMessage.length);
   		        serOut.writeObject(saveMessage);
   		        serOut.close();
			}
			if (userCmd == "decrypt") {
				message.setText("");
				c.init(Cipher.DECRYPT_MODE, key, codeSpec);
				byte[] decodedMessage = c.doFinal(getCodedContent());
				System.out.println(new String(decodedMessage));
				message.setText(new String(decodedMessage));
			}
		} catch (NoSuchAlgorithmException ex) {
			System.out.println(ex);
		} catch (InvalidAlgorithmParameterException ex) {
			System.out.println(ex);
		} catch (InvalidKeyException ex) {
			System.out.println(ex);
		} catch (InvalidKeySpecException ex) {
			System.out.println(ex);
		} catch (NoSuchPaddingException ex) {
			System.out.println(ex);
		} catch (BadPaddingException ex) {
			System.out.println("BadPadding:Invalid password?");
		} catch (IllegalBlockSizeException ex) {
			System.out.println(ex);
   		} catch (IOException ioex) {
   			System.out.println(ioex);
  		}
	}
}