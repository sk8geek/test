import java.util.regex.Pattern;

public class TestRegex {
    
    public static void main(String[] args) {
        if (Pattern.matches("[a-zA-Z]\\w\\w\\w", args[0])) {
            System.out.println("true");
        } else {
            System.out.println("false");
        }
    }
}
