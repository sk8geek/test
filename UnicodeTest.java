public class UnicodeTest {

	public static void main(String[] args) {
		String firstElement = args[0];
		String uniString = "\u00F6";
		System.out.println("String = " + firstElement);
		System.out.println("Length= " + firstElement.length());
		System.out.println("Unicode= " + uniString);
		System.out.println("Length= " + uniString.length());
	}
}
