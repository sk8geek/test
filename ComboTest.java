import java.awt.*;
import javax.swing.*;

public class ComboTest extends JFrame {

	public static void main(String[] args) {
		ComboTest c = new ComboTest();
	}


	ComboTest() {
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
	
		JComboBox test = new JComboBox();
		
		test.addItem("Chocolate");
		test.addItem("Vanilla");
		test.addItem("Coffee");
		test.addItem("Ripple");
		test.setEditable(false);

		Container cp = getContentPane();
		cp.add(test);
		pack();
		show();
	}
}
