import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class LayeredFrame extends JFrame {

	public static void main(String[] a){
		LayeredFrame layerDemo = new LayeredFrame();
	}
	
	LayeredFrame() {
		// user interface
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("SimpleJFrame");
		Container cp = getContentPane();
		JLayeredPane lp = getLayeredPane();

		// content pane
		cp.setLayout(new FlowLayout());
		cp.add(new JLabel("this is the content pane"));
		cp.add(new JButton("Press"));
		cp.add(new JTextField(8));
		
		// internal frames
		JInternalFrame jif_default = 
			new JInternalFrame("default", true, true, true, true);
		jif_default.getContentPane().add(new JTextArea(4,20));
		jif_default.pack();
		jif_default.setVisible(true);
				
		JInternalFrame jif_modal = new JInternalFrame();
		jif_modal.getContentPane().add(new JLabel("head & shoulders"));
		jif_modal.pack();
		jif_modal.setVisible(true);

		// layered pane
		lp.add(jif_default, JLayeredPane.DEFAULT_LAYER);
		lp.add(jif_modal, JLayeredPane.MODAL_LAYER);
		jif_modal.setLocation(60,60);
		
		setSize(400,300);
		show();
	}
}