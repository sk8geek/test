import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class ScrollFlowTest extends JFrame {

	public static void main(String[] a){
		ScrollFlowTest s = new ScrollFlowTest();
	}
	
	ScrollFlowTest() {
		// user interface
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("ScrollFlowTest");
//		setSize(new Dimension(140,140));

		ScrollableJPanel p = new ScrollableJPanel();
//		p.setPreferredSize(new Dimension(130,600));
		
		JButton b_t1 = new JButton("Test1");
		JButton b_t2 = new JButton("Test2");
		JButton b_t3 = new JButton("Test3");
		JButton b_t4 = new JButton("Test4");
		JButton b_t5 = new JButton("Test5");
		JButton b_t6 = new JButton("Test6");
		JButton b_t7 = new JButton("Test7");
		JButton b_t8 = new JButton("Test8");

//		p.setPreferredSize(new Dimension(100,100));

		
		p.add(b_t1);
		p.add(b_t2);
		p.add(b_t3);
		p.add(b_t4);
		p.add(b_t5);
		p.add(b_t6);
		p.add(b_t7);
		p.add(b_t8);

		JScrollPane sp = new JScrollPane(p);
System.out.println("x1");
//		p.revalidate();
/*
		JScrollPane sp = new JScrollPane(p, 
			JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
			JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
*/
//		JViewport vp = sp.getViewport();
//		vp.setLayout(new FlowLayout());

		

//		sp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
//		sp.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

		getContentPane().add(sp);
System.out.println("x2");
		
//		JViewport vp = sp.getViewport();
//		vp.setViewSize(new Dimension(150,100));

		pack();
System.out.println("x3");

//		sp.getViewport().setPreferredSize(getSize());
		show();
	}

}

class ScrollableJPanel extends JPanel implements Scrollable {

	private Dimension getPreferredHeight(int availableWidth){
		Component[] children = getComponents();
System.out.println(children.length);
		int totalHeight=0, totalWidth=0;
		for (int c=0; c<children.length; c++) {
			totalHeight += children[0].getPreferredSize().getHeight();
			totalWidth += children[0].getPreferredSize().getWidth();
		}
System.out.println(totalWidth);
		double meanHeight = totalHeight / children.length;
		double meanWidth = totalWidth / children.length;
		int approxWidth = availableWidth;
		if ( approxWidth == 0 ) {
			approxWidth = (int)(meanWidth * 2);
		}
		int requiredRowCount = (int)((totalWidth / approxWidth) + 0.5);
		int requiredHeight = (int)(meanHeight * requiredRowCount);
System.out.println("avW=" + availableWidth);
System.out.println("apW=" + approxWidth);
System.out.println("rqH=" + requiredHeight);
		setPreferredSize(new Dimension(approxWidth, requiredHeight));
		return new Dimension(approxWidth, requiredHeight);
	}

	public Dimension getPreferredScrollableViewportSize() {
System.out.println("parent's width=" + getParent().getWidth());
		Dimension d = getPreferredHeight(getParent().getWidth());
System.out.println(d);
		return d;
	}

	public int getScrollableBlockIncrement(Rectangle vis, int orientation, 
		int direction){
		return 1;
	}
	
	public boolean getScrollableTracksViewportHeight() {
		return false;
	}
	
	public boolean getScrollableTracksViewportWidth() {
		return true;
	}
	
	public int getScrollableUnitIncrement(Rectangle vis, int orientation,
		int direction){
		return 1;
	}
}