import java.io.*;
import java.sql.*;
import java.text.*;
import java.util.*;
import java.awt.*;
import javax.swing.*;
import java.net.*;
import javax.mail.*;
import com.sun.mail.smtp.*;
import com.mysql.jdbc.*;


public class MailTest extends JFrame {

	private Properties mailServer = new Properties();

	public static void main(String[] args) {
		MailTest m = new MailTest();
	}
	
	MailTest() {
		mailServer.setProperty("mail.transport.protocol", "smtp");
		mailServer.setProperty("mail.host", "bronze");
		mailServer.setProperty("mail.user", "crb.watcher");
		sendTestMail();
	}
	
	private void sendTestMail() {
		Session mailSession = Session.getInstance(mailServer);
		SMTPTransport mailTrans = new SMTPTransport(mailSession, 
			new URLName("smtp", "bronze", -1, "", "", ""));
		SMTPMessage msg = new SMTPMessage(mailSession);
		try {
			msg.addRecipients(Message.RecipientType.TO, 
				"steven@channel-e.co.uk");
			msg.setSubject("From Russia with love");
			msg.setText("Hello Big Boy, fancy some nookie?", "UTF8");
			mailTrans.send(msg);
		} catch (MessagingException mex) {
			System.out.println(mex);
		}
	}
}