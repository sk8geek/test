import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class ClickPass extends JFrame implements ActionListener {

	private JTextField lastButtonPressed = new JTextField("none");

	public static void main(String[] a){
		ClickPass cleanFrame = new ClickPass();
	}
	
	ClickPass() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("ClickPass");
		JDesktopPane dtp = new JDesktopPane();
		setContentPane(dtp);

		JButton b_one = new JButton("One");
		b_one.setActionCommand("one");
		b_one.addActionListener(this);
		JButton b_two = new JButton("Two");
		b_two.setActionCommand("two");
		b_two.addActionListener(this);
		
		JInternalFrame jif_one = new JInternalFrame("One");
			jif_one.getContentPane().add(b_one);
			jif_one.pack();
			dtp.add(jif_one);
			jif_one.setLocation(20, 20);
			jif_one.setVisible(true);

		JInternalFrame jif_two = new JInternalFrame("Two");
			jif_two.getContentPane().add(b_two);
			jif_two.pack();
			dtp.add(jif_two);
			jif_two.setLocation(100, 20);
			jif_two.setVisible(true);

		JInternalFrame indicator = new JInternalFrame("Indicator");
			indicator.getContentPane().add(lastButtonPressed);
			indicator.pack();
			dtp.add(indicator);
			indicator.setLocation(70, 90);
			indicator.setVisible(true);
		
		setSize(200, 180);
		setVisible(true);
	}
	
	public void actionPerformed(ActionEvent evnt) {
		lastButtonPressed.setText(evnt.getActionCommand());
	}
}