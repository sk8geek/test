import java.awt.*;
import java.applet.*;
import javax.swing.*; // this will be clear later

public class Banner extends JApplet implements Runnable {

	private Thread bannerThread = null;
 	private int x = 10;
	private JPanel needThis;

 	public void init() {
 		final Dimension appletSize = getSize();
 		needThis = new JPanel(){
 			protected void paintComponent(Graphics g) {
 				super.paintComponent(g);
 	 			x += 5;
 				if (x > (appletSize.width - 10)) {
 					x = 10;
 				}
 				g.setFont(new Font("Serif" , Font.BOLD , 24));
 				g.setColor(Color.BLUE);
 				g.drawString("Why a duck . . why a no chicken?", x, 50);
 			}
 		};
 		needThis.setBackground(Color.yellow);
 		getContentPane().add(needThis);
 	}

 	public void start() {
 		if (bannerThread == null) {
 			bannerThread = new Thread(this);
 			bannerThread.start();
 		}
 	}

 	public void run() {
 		Thread myThread = Thread.currentThread();
 		while (bannerThread == myThread) {
 			try {
 				Thread.sleep(100);
 			} catch (InterruptedException e) {}
 			needThis.repaint();
 		}
 	}

	public void stop() {
 		bannerThread = null;
 	}
}