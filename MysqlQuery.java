import java.util.*;
import java.sql.*;


public class MysqlQuery { 

   public static void main ( String[] args ) {
   
      if ( args.length == 0 ) {
         System.out.println("No username/password specified.");
         return;
      }
      
      try {
      
         DriverManager.registerDriver(new com.mysql.jdbc.Driver());
         java.sql.Connection con = 
            DriverManager.getConnection("jdbc:mysql://csdb:3306/itres",
            args[0],args[1]);
         java.sql.Statement smnt = con.createStatement();
         java.sql.ResultSet res = smnt.executeQuery("SELECT * FROM register");
         
         while (res.next()) {
            
            System.out.print(res.getString("uname") + " ");
            System.out.println(res.getTimestamp("lastmod").getTime());
         }
         
         res.close();
         smnt.close();
         con.close();
         
      }
      
      catch (SQLException sqlex) {
      
         String msg = new String(sqlex.getMessage());
         
         System.out.println("SQL Exception:" + msg);

         // Test for invalid logon details.  IF this is the cause of the exception
         // then don't bother with the stack trace.
         
         if ( msg.indexOf("logon denied") == 0 ) {
            sqlex.printStackTrace(System.out);
         }
      
      }
   }
}
