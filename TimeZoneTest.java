import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.Calendar;
import java.util.TimeZone;
import java.text.DateFormat;


public class TimeZoneTest extends JFrame {

	private JTextField LocalTime = new JTextField(8);
	private JTextField GMT = new JTextField(8);
	private JTextField NewYorkTime = new JTextField(8);
	private JTextField TokyoTime = new JTextField(8);
	
	public static void main(String[] a){
		TimeZoneTest tzt = new TimeZoneTest();
	}
	
	TimeZoneTest() {
		userInterface();
		// set times to now
		DateFormat showTime = DateFormat.getTimeInstance(DateFormat.MEDIUM);
		Calendar localCalendar = Calendar.getInstance();
		LocalTime.setText(showTime.format(localCalendar.getTime()));
		// do other time zones

		showTime.setTimeZone(TimeZone.getTimeZone("GMT"));
		GMT.setText(showTime.format(localCalendar.getTime()));

		showTime.setTimeZone(TimeZone.getTimeZone("America/New_York"));
		NewYorkTime.setText(showTime.format(localCalendar.getTime()));

		showTime.setTimeZone(TimeZone.getTimeZone("Asia/Tokyo"));
		TokyoTime.setText(showTime.format(localCalendar.getTime()));

		String[] timeZoneIDList = 
			TimeZone.getTimeZone("GMT").getAvailableIDs();
		for (int i = 0; i < timeZoneIDList.length; i++) {
			if (timeZoneIDList[i].indexOf("BST") != -1 ||
				timeZoneIDList[i].indexOf("ondon") != -1 ||
				timeZoneIDList[i].indexOf("UK") != -1 ||
				timeZoneIDList[i].indexOf("ngland") != -1 ||
				timeZoneIDList[i].indexOf("ingdom") != -1) {
				System.out.println(timeZoneIDList[i]);
			}
		}
	}
	
	private void userInterface() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("TimeZoneTest");
		Container cp = getContentPane();
		JPanel p = new JPanel(new GridLayout(4, 2));
		p.add(new JLabel("Local time"));
		p.add(LocalTime);
		p.add(new JLabel("GMT time"));
		p.add(GMT);
		p.add(new JLabel("N.York time"));
		p.add(NewYorkTime);
		p.add(new JLabel("Tokyo time"));
		p.add(TokyoTime);
		getContentPane().add(p);
		pack();
		setVisible(true);
	}
}