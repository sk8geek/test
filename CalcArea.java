public class CalcArea {

	public static void main(String[] args) {
		if (args[0] != null) {
			double radius = Double.parseDouble(args[0]);
			double area = Math.PI * radius * radius;
			System.out.println("Area=" + area);
		}
	}
}