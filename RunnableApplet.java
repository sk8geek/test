import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.applet.*;


public class RunnableApplet extends JApplet implements Runnable {

	private Thread runMe;
	private boolean stillKickin = false;

	private JTextArea t;
	
	public void init() {
		userInterface();
		stillKickin = true;
	}

	
	public void start() {
		runMe = new Thread(this);
		runMe.setPriority(Thread.MIN_PRIORITY);
		runMe.start();
	}


	public void stop() {
		stillKickin = false;
	}


	public void run() {
		while (stillKickin) {
			try {
				runMe.sleep(1000);
			}
			catch (InterruptedException iex){
				// no action taken
			}
			if (t.getText().length() < 32 ) {
				t.setText(t.getText() + "\nswimming,");
			} else {
				t.setText("Just keep swimming,");
			}
		}
	}

	
	public void destroy() {
//		stillKickin = false;
	}


	private void userInterface () {
		t = new JTextArea("Just keep swimming,",4,20);
		getContentPane().add(t);
	}
}