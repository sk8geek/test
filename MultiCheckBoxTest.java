import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class MultiCheckBoxTest extends JFrame {

	public static void main(String[] a){
		MultiCheckBoxTest cleanFrame = new MultiCheckBoxTest();
	}
	
	MultiCheckBoxTest() {
		// user interface
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("SimpleJFrame");
		Container cp = getContentPane();
		cp.setLayout(new FlowLayout());
        cp.add(new JCheckBox("CheckBox 1"));
        cp.add(new JCheckBox("CheckBox 2"));
        cp.add(new JCheckBox("CheckBox 3"));


		cp.add(new JButton("Press"));
		cp.add(new JTextField(8));
		pack();
		setVisible(true);
	}
}
