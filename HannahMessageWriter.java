import java.util.*;
import java.sql.*;
import com.mysql.jdbc.Driver;


public class HannahMessageWriter { 

    public static void main (String[] args) {
        if (args.length == 0) {
            System.out.println("No username/password specified.");
            return;
        }
        try {
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            Connection con = DriverManager.getConnection("jdbc:mysql://csdb:3306/test", args[0],args[1]);
            PreparedStatement smnt = con.prepareStatement("SELECT * FROM decoded_msg_view");
            ResultSet res = smnt.executeQuery();
            System.out.println();
            while (res.next()) {
                System.out.print(res.getString("real_value"));
            }
            System.out.println("\n");
            res.close();
            smnt.close();
            con.close();  
        } catch (SQLException sqlex) {
            System.out.println(sqlex);
        }
    }
}
