import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class SplitFrame extends JFrame {

	public static void main(String[] a){
		SplitFrame s = new SplitFrame();
	}
	
	SplitFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("SplitFrame");

		JPanel lhs = new JPanel();
		lhs.setPreferredSize(new Dimension(210,480));
		JPanel rhs = new JPanel();
		rhs.setPreferredSize(new Dimension(450,480));
		rhs.setBackground(Color.pink);
		
		Container cp = getContentPane();
		cp.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
		cp.add(lhs);
		cp.add(rhs);

		// add any old stuff to the panels
		lhs.add(new JButton("ButtonA"));
		rhs.add(new JButton("ButtonB"));
		lhs.add(new JButton("ButtonC"));
		rhs.add(new JButton("ButtonD"));
		
		rhs.add(new JTextArea(4,20));
		
		JPanel g = new JPanel(new GridLayout(2,2));
		g.add(new JButton("Button1"));
		g.add(new JButton("Button2"));
		g.add(new JButton("Button3"));
		g.add(new JButton("Button3"));
		lhs.add(g);
		// end adding of stuff
		
		pack();		
		setVisible(true);
	}
}