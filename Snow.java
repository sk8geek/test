import java.awt.*;
import java.applet.*;
import java.net.*;

public class Snow extends Applet {

  Image f1;

  public void init() {
    URL imgSrc;
    setBackground(new Color(0,0,0));
    Graphics g = getGraphics();
    try {
      imgSrc = new URL("http://bronze/images");
      f1 = getImage(imgSrc, "flake1.gif");
    }
    catch (MalformedURLException muex) {
    }
  }
  
  public void paint(Graphics g) {
    g.drawImage(f1, 20, 20, this);
  }
  
  public void start() {
    repaint();
  }
  
  public void stop() {
  }
  
  public void destroy() {
  }
}
