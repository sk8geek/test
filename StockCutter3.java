import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;

public class StockCutter3 extends JFrame{

	private JTextField stockLength = new JTextField("10", 4);
	private int stockLen = 0;
	private JTextField pieceLength = new JTextField("", 4);
	private JTextField pieceCount = new JTextField("", 4);
	private int s = 0;
	private int W = 0;
	private ArrayList pieces = new ArrayList();
	private	JButton addPiece = new JButton("Add");
	private	JButton clearPieces = new JButton("Clear");
	private	JButton calculate = new JButton("Calculate");
	private ArrayList buckets = new ArrayList();

	public static void main(String args[]) {
		StockCutter3 s = new StockCutter3();
	}

	StockCutter3() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Stock Cutter 3");
		userInterface();
	}
	
	private void addPieces() {
		int numberOf = Integer.parseInt(pieceCount.getText());
		int lengthOf = Integer.parseInt(pieceLength.getText());
		for (int i = 0; i < numberOf; i++) {
			pieces.add(new Integer(lengthOf));
		}
	}
	
	private void startCalculation() {
		int initialBucketCount = sortPieces();
		System.out.println("Estimated stock requrd:" + initialBucketCount);
		buckets.clear();
		buckets.ensureCapacity(initialBucketCount);
		// set bucket capacities
		stockLen = Integer.parseInt(stockLength.getText());
		for (int i = 0; i < initialBucketCount; i++) {
			buckets.add(new Bucket(stockLen));
//			((Bucket)buckets.get(i)).setCapacity(10);
		}
		shareOutPieces();
	}
	
	private int sortPieces() {
		int totalLength = 0;
		Object[] sortablePieces = pieces.toArray();
		Arrays.sort(sortablePieces);
//System.out.print("sortablePieces:");
//for (int i = 0; i<sortablePieces.length; i++) {
//System.out.print(((Integer)sortablePieces[i]).toString() + ", ");
//}
//System.out.println(".");
		/* The array is actually in the reverse of the order that I want
		   as I'd like biggest piece first.  
		   Also, as I want to be able to take 
		   elements out I move the whole lot back to an ArrayList. */
		pieces.clear();
System.out.print("pieces:");
		for (int i = sortablePieces.length - 1; i >= 0; i--) {
			pieces.add(sortablePieces[i]);
System.out.print(sortablePieces[i] + ", ");
			totalLength += ((Integer)sortablePieces[i]).intValue();
		}
		System.out.println("Total length requested:" + totalLength);
System.out.println(".");
		double stockLenDbl = Double.parseDouble(stockLength.getText());
		double bucketsReqd = (double)totalLength / stockLenDbl;
		if (bucketsReqd != (int)bucketsReqd) {
			bucketsReqd ++;
		}
		System.out.println("Minimum waste         :" + (double)totalLength % stockLenDbl);
		return (int)bucketsReqd;
	}
	
	private void shareOutPieces() {
		int queryType = 0;
		int bucketPointer = 0;
		int startPiece = 0;
		int piece = 0;
		int loopCounter = 0;
	piecesremain:
		while (pieces.size() > 0) {
			// while we still have piece to cut 
			startPiece = ((Integer)pieces.get(0)).intValue();
System.out.println("start piece=" + startPiece);
//			piece = ((Integer)pieces.get(0)).intValue();
		newsize:
			while ((piece = ((Integer)pieces.get(0)).intValue()) == startPiece) {
				// while we still have the same piece size
System.out.print("piece size=" + piece + "    ");
System.out.print("PIECES:");
for (int i = 0; i<pieces.size(); i++) {
	System.out.print(((Integer)pieces.get(i)).toString() + ", ");
}
System.out.println(".");
			// EXACT FIT then WILL TAKE loops 
				for (int bucketOp = Bucket.EXACT_FIT; bucketOp < Bucket.SWAP_WITH; 
					bucketOp++ ){ // this only does two loops 
System.out.println("bucketOp=" + bucketOp);
					int timesRejected = 0;
					while (timesRejected < buckets.size()) {
					// offer as exact fit to all buckets
						Bucket currentBucket = (Bucket)buckets.get(bucketPointer);
System.out.print(bucketPointer + "=" + currentBucket.reportState() + "    ");
						if (currentBucket.doYouWant(piece, bucketOp)) {
							currentBucket.acceptPiece(piece);
System.out.println("<--" + currentBucket.reportState());
							pieces.remove(0);
							timesRejected = 0; // XXX
							if (pieces.size() == 0) {
								break newsize;
							}
							if (((Integer)pieces.get(0)).intValue() != startPiece) {
								bucketPointer ++;
								if (bucketPointer >= buckets.size()) {
									bucketPointer = 0;
								}
								continue piecesremain;
							}
						} else {
							timesRejected++;
System.out.println("reject times=" + timesRejected);
						}
						bucketPointer ++;
						if (bucketPointer >= buckets.size()) {
							bucketPointer = 0;
						}
					}
				}
			// SWAP WITH 
//System.out.println("swap mode");
				int timesRejected = 0;
				int bucketOp = Bucket.SWAP_WITH;
				while (timesRejected < buckets.size()) {
System.out.print("REMAINING: ");
for (int j = 0; j < pieces.size(); j++ ) {
	System.out.print(((Integer)pieces.get(j)).toString() + ", ");
}
System.out.println(".");
					Bucket currentBucket = (Bucket)buckets.get(bucketPointer);
					piece = ((Integer)pieces.get(0)).intValue();
System.out.print(bucketPointer + "=" + currentBucket.reportState() + "    ");
					if (currentBucket.doYouWant(piece, bucketOp)) {
						int swappedOutPiece = currentBucket.swapPiece(piece);
System.out.println("Swapped " + piece + " in exchange for " + swappedOutPiece);
						pieces.remove(0);
						timesRejected = 0; // XXX
						if (swappedOutPiece != 0) {
							pieces.add(new Integer(swappedOutPiece));
						}
						if (pieces.size() == 0) {
							break newsize;
						}
					} else {
						timesRejected++;
					}
					bucketPointer ++;
					if (bucketPointer >= buckets.size()) {
						bucketPointer = 0;
					}
				}
				break newsize;
			}
			if (pieces.size() > 0) {
				buckets.add(new Bucket(stockLen));
			}
		}
		int actualWaste = 0;
		for (int i = 0; i < buckets.size(); i++) {
			System.out.println(((Bucket)buckets.get(i)).reportState());
			actualWaste += ((Bucket)buckets.get(i)).getRemaining();
		}
		System.out.println("Actual stock used     :" + buckets.size());
		System.out.println("Actual waste remaining:" + actualWaste);
		System.out.println("DONE");
	}
	
	private void userInterface() {
	
		JPanel p1 = new JPanel();
		p1.add(new JLabel("Stock length"));
		p1.add(stockLength);
		
		JPanel p2 = new JPanel();
		p2.add(clearPieces);
		p2.add(new JLabel("Piece length"));
		p2.add(pieceLength);
		p2.add(new JLabel(" Qty"));
		p2.add(pieceCount);
		p2.add(addPiece);
		
		Container cp = getContentPane();
		cp.add(p1, BorderLayout.NORTH);
		cp.add(p2);
		cp.add(calculate, BorderLayout.SOUTH);
		
		clearPieces.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evnt) {
				pieces.clear();
				buckets.clear();
				System.out.println("Cleared");
			}
		});
		
		addPiece.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evnt) {
				setFieldEditing(false);
				addPieces();
				setFieldEditing(true);
			}
		});
		
		calculate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evnt) {
				setFieldEditing(false);
				startCalculation();
				setFieldEditing(true);
			}
		});
		JPanel buttonPanel = new JPanel();
		buttonPanel.add(calculate);
		getContentPane().add(buttonPanel, BorderLayout.SOUTH);
		
		pack();
		setVisible(true);
	}
	
	private void setFieldEditing(boolean editable) {
		stockLength.setEnabled(editable);
		pieceLength.setEnabled(editable);
		pieceCount.setEnabled(editable);
		clearPieces.setEnabled(editable);
		addPiece.setEnabled(editable);
		calculate.setEnabled(editable);
	}
}

class Bucket {

	private int capacity = 0;
	private int remaining = 0;
	private ArrayList parts = new ArrayList();
	private boolean bucketFullToBrim = false;
	
	static final int EXACT_FIT = 1;
	static final int WILL_TAKE = 2;
	static final int SWAP_WITH = 3;
	
	Bucket() {}
	
	Bucket(int canHold) {
		setCapacity(canHold);
	}
	
	void setCapacity(int size) {
		if (calcPartsLength() <= size) {
			capacity = size;
		} else {
			capacity = calcPartsLength();
		}
		calcPartsLength();
	}
	
	static int nextOp(int currentOp) {
	 	int nextOperation = currentOp + 1;
		if (nextOperation > 3) {
	 		nextOperation = 3;
	 		System.exit(0);
	 	}
	 	return nextOperation;
	}
	
	void acceptPiece(int pieceLen) {
		parts.add(new Integer(pieceLen));
		if (calcPartsLength() == capacity) {
			bucketFullToBrim = true;
//			System.out.println(reportState());
		}
	}
	
	int swapPiece(int pieceLen) {
		if (pieceLen <= remaining) {
			parts.add(new Integer(pieceLen));
			if (calcPartsLength() == capacity) {
				bucketFullToBrim = true;
//				System.out.println(reportState());
			} else {
				bucketFullToBrim = false;
			}
			return 0;
		}
		int swapPieceIdx = smallestToSwap(pieceLen);
		int swapPiece = ((Integer)parts.get(swapPieceIdx)).intValue();
//System.out.print("[" + remaining + "] ");
		parts.remove(swapPieceIdx);
		parts.add(new Integer(pieceLen));
		if (calcPartsLength() == capacity) {
			bucketFullToBrim = true;
//			System.out.println(reportState());
		} else {
			bucketFullToBrim = false;
		}
//System.out.println("accepted " + pieceLen + " in exchange for " + swapPiece);
		return swapPiece;
	}

	String reportState() {
		StringBuffer bucketState = new StringBuffer();
		bucketState.append("[" + remaining + "] ");
		for (int i = 0; i < parts.size(); i++ ) {
			bucketState.append(((Integer)parts.get(i)).toString() + ", ");
		}
		bucketState.append(".");
		return bucketState.toString();
	}
	
	int getRemaining() {
		return remaining;
	}
	
	private int smallestToSwap(int pieceLen) {
		int smallest = capacity;
		int pieceIdx = 0;
		for (int i = 0; i < parts.size(); i++) {
			int thisPart = ((Integer)parts.get(i)).intValue();
			if ( thisPart < smallest && thisPart != pieceLen) {
				smallest = ((Integer)parts.get(i)).intValue();
				pieceIdx = i;
			}
		}
		return pieceIdx;
	}
	
	boolean doYouWant(int pieceLen, int bucketOp) {
//System.out.print("bucket offered " + pieceLen + " for operation " + bucketOp + ".  Result=");
		if (bucketFullToBrim) {
//System.out.println("rejected");
			return false;
		}
		if (bucketOp == EXACT_FIT && remaining == pieceLen) {
//System.out.println("accepted");
			return true;
		}
		if (bucketOp == WILL_TAKE && remaining >= pieceLen) {
//System.out.println("accepted");
			return true;
		}
		if (bucketOp == SWAP_WITH) {
			if (parts.size() == 1 && (remaining < ((Integer)parts.get(0)).intValue())) {
//System.out.println("rejected");
				return false;
			}
			for (int i = 0; i < parts.size(); i++) {
				if (((Integer)parts.get(i)).intValue() != pieceLen) {
//System.out.println("accepted");
					return true;
				}
			}
		}
//System.out.println("rejected");
		return false;
	}
		
	private int calcPartsLength() {
		int lengthOfParts = 0;
		for (int i = 0; i < parts.size(); i++) {
			lengthOfParts += ((Integer)parts.get(i)).intValue();
		}
		remaining = capacity - lengthOfParts;
		return lengthOfParts;
	}

}