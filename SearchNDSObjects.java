import java.net.*;
import javax.naming.*;
import java.util.*;
import com.novell.ldap.*;

public class SearchNDSObjects {
	
	public static void main(String[] args) {
		if (args.length == 0) {
			showHelp();
			System.exit(0);
		}
		String LDAPServer = args[1];
		String LDAPbasedn = args[2];
		String LDAPfilter = args[3];
		System.out.println("Performing LDAP search against: " + LDAPServer);
		System.out.println("Starting search in domain: " + LDAPbasedn);
		System.out.println("Filtering for: " + LDAPfilter);
		try {
			LDAPConnection LDAPCon = new LDAPConnection();
			LDAPCon.connect(LDAPServer,389);
			LDAPCon.bind(LDAPConnection.LDAP_V3,"",new byte[0]);
			if (args[0].equalsIgnoreCase("schema")) {
				System.out.println("Scheme follows:");
				LDAPSchema schema = 
					LDAPCon.fetchSchema(LDAPCon.getSchemaDN());
				for (Enumeration e = schema.getObjectClassNames(); 
					e.hasMoreElements() ;) {
					System.out.println((String)e.nextElement());
				}
			} else {
				System.out.println("Search starts");
				String[] attribs = new String[] {"cn","surname","givenName","LID"};
				LDAPSearchResults results = 
					LDAPCon.search(LDAPbasedn, 
					LDAPConnection.SCOPE_SUB, 
					LDAPfilter, attribs, false);
				int itemCount = 0;
				while ( results.hasMore() ) {
					itemCount++;
					LDAPEntry e = results.next();
					LDAPAttributeSet as = e.getAttributeSet();
					Iterator i = as.iterator();
					while (i.hasNext()) {
						LDAPAttribute thisAttr = (LDAPAttribute)i.next();
						String attrName = thisAttr.getName();
						String attrValue = thisAttr.getStringValue();
						System.out.print(attrValue + ",");
					}
					System.out.println();
				}
				System.out.println("Item count=" + itemCount);
			}
			LDAPCon.disconnect();
		} catch (LDAPException ldapex) {
			System.out.println("LDAP error: " + ldapex);
    	}
	}
	
	static void showHelp() {
		System.out.println("Arguments must be given in the order:");
		System.out.println("queryType server basedn filter");
	}
}
