import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.net.*;
import javax.xml.parsers.*;
import org.xml.sax.*;

public class RSSReader extends JFrame 
	implements ActionListener {

	public static void main(String[] a){
		RSSReader cleanFrame = new RSSReader();
	}
	
	RSSReader() {
		// user interface
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("RSS Reader");
		Container cp = getContentPane();
		JTextField source = new JTextField(20);
		JLabel l_src = new JLabel("Source:");
		l_src.setLabelFor(source);
		cp.add(source);
		JButton startParse = new JButton("Parse");
		cp.add(startParse);

		startParse.addActionListener(this);

		pack();
		show();
	}
	
	public void actionPerformed(ActionEvent e) {
	
		
	}
}