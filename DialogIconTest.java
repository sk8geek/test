import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class DialogIconTest extends JFrame {

	private JComboBox c_first = new JComboBox();
	private JComboBox c_second = new JComboBox();
	private JComboBox c_third = new JComboBox();
	
	public static void main(String[] a){
		DialogIconTest d = new DialogIconTest();
	}
	
	DialogIconTest() {
		final JFrame me = this;
		// user interface
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("DialogIconTest");
		Container cp = getContentPane();
		Toolkit awTk = Toolkit.getDefaultToolkit();
	    ImageIcon ii = new ImageIcon(awTk.getImage("arrow.png"));

		JInternalFrame jif = new JInternalFrame("hello");
		JButton b_openDialog = new JButton("press here");
		jif.getContentPane().add(b_openDialog);
		jif.pack();
		jif.setVisible(true);

		cp.add(jif);
		pack();
		setVisible(true);
		
		b_openDialog.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evnt){
				JDialog d_testIcon = new JDialog(me, "Dialog");
				d_testIcon.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
//				d_testIcon.setFrameIcon(
//					(JInternalFrame)getParent().getFrameIcon());
				JButton b_doNothing = new JButton("Don't press");
				d_testIcon.getContentPane().add(b_doNothing);
				d_testIcon.pack();
				d_testIcon.setVisible(true);
			}
		});
	}
}