import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class CloseMeTest extends JFrame {

	private JFrame myDad;

	public static void main(String[] a) {
		CloseMeTest c = new CloseMeTest();
		c.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}
	
	
	CloseMeTest() {
		Container cp = getContentPane();
		cp.setLayout(new FlowLayout());
		JButton b_open = new JButton("Open");
		b_open.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evnt){
				createChildFrame();
			}
		});
		
		JButton b_close = new JButton("Close");
		b_close.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent evnt) {
	    	if (myDad != null) {
	    		myDad.setVisible(true);
	    	}
      	dispose();
      }
    });
		
		cp.add(b_open);
		cp.add(b_close);

    addWindowListener(new WindowAdapter() {
	    public void windowClosing(WindowEvent evnt) { 
	    	if (myDad != null) {
	    		myDad.setVisible(true);
	    	}
      	dispose();
	    }
	  });

		pack();
		show();
	}


	CloseMeTest(JFrame parent) {
		this();
		this.myDad = parent;
	}


	private void createChildFrame() {
		CloseMeTest x = new CloseMeTest(this);
		setVisible(false);
	}
}


