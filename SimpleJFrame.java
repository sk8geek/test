import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class SimpleJFrame extends JFrame {

	public static void main(String[] a){
		SimpleJFrame cleanFrame = new SimpleJFrame();
	}
	
	SimpleJFrame() {
		// user interface
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("SimpleJFrame");
		Container cp = getContentPane();
		cp.setLayout(new FlowLayout());

		cp.add(new JButton("Press"));
		cp.add(new JTextField(8));
		pack();
		show();
	}
}