import javax.swing.*;
import java.applet.*;


public class SimpleApplet extends Applet {

	public void init() {
		String[] colours = new String[] {
			"Red", "Orange", "Yellow", "Green", "Blue", "Indigo", "Violet"
		};
		JTree tree = new JTree(colours);
		add(new JScrollPane(tree));
	}
}