import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.applet.*;


public class AppletPanels extends JApplet {

	private JPanel panelOne = new JPanel();
	private JPanel panelTwo = new JPanel();
	private JPanel buttonPanel = new JPanel();
	private Container cp;
	private boolean panelOneShowing = true;

	public void init() {
		// create applet with panelOne showing
		cp = getContentPane();
		
		JLabel labelOne = new JLabel("Panel One");
		panelOne.add(labelOne);
		JLabel labelTwo = new JLabel("Panel Two");
		panelTwo.add(labelTwo);
		JButton swapPanels = new JButton("Swap panels");
		buttonPanel.add(swapPanels);
		cp.add(panelOne);
		panelOneShowing = true;
		cp.add(buttonPanel, BorderLayout.SOUTH);
		swapPanels.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evnt) {
				swapPanels();
			}
		});
	}

	private void swapPanels() {
		cp.removeAll();
		if (panelOneShowing) {
			cp.add(panelTwo);
			cp.add(buttonPanel, BorderLayout.SOUTH);
			panelOneShowing = false;
		} else {
			cp.add(panelOne);
			cp.add(buttonPanel, BorderLayout.SOUTH);
			panelOneShowing = true;
		}
		cp.validate();
		repaint();
	}
	
}