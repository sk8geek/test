import java.lang.*;


public class CommandLineTest {
    public static void main(String[] clargs) {
        long total = 0;

        String bob = new String("Case");
        if ( clargs.length == 2 ) {
            try {
                total = Long.parseLong( clargs[0] );
                System.out.print ( clargs[0] );
            }
            catch(NumberFormatException e) {
                System.out.print ( "xxx" );
            }
            System.out.print ( " + " );

            try {
                total = total + Long.parseLong( clargs[1] );
                System.out.print ( clargs[1] );
            }
            catch(NumberFormatException e) {
                System.out.print ( "yyy" );
            }
        }
        System.out.println( " = " + total );
    }
}