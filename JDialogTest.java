import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class JDialogTest extends JFrame implements ActionListener {

	private JDialog dialog;

	public static void main(String[] a){
		JDialogTest cleanFrame = new JDialogTest();
	}
	
	JDialogTest() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("JDialogTest");
		JButton newNormal = new JButton("Normal");
		newNormal.addActionListener(this);
		newNormal.setActionCommand("normal");
		JButton newClean = new JButton("Clean");
		newClean.addActionListener(this);
		newClean.setActionCommand("clean");

		JPanel panel = new JPanel();
		panel.add(newNormal);
		panel.add(newClean);
		getContentPane().add(panel);

		pack();
		setVisible(true);
	}
	
	public void actionPerformed(ActionEvent evnt) {
		if (evnt.getActionCommand() == "close") {
			dialog.dispose();
		} else {
			dialog = new JDialog(this, "Normal", true);
			JButton closeMe = new JButton("Close me");
			closeMe.addActionListener(this);
			closeMe.setActionCommand("close");
			JPanel panel = new JPanel();
			panel.setPreferredSize(new Dimension(200, 100));
			panel.add(closeMe);
			dialog.getContentPane().add(panel);
			if (evnt.getActionCommand() == "clean") {
				dialog.setTitle("Clean");
				dialog.setUndecorated(true);
			} 
			dialog.pack();
			dialog.setLocation(this.getWidth() + 10, this.getHeight() + 10);
			dialog.setVisible(true);
		}
	}
}