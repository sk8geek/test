import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class ModalJInternalFrame extends JFrame {

	private final JInternalFrame modalJIF = new JInternalFrame("Modal");
	private final Component newPLayer;
	
	public static void main(String[] a){
		ModalJInternalFrame cleanFrame = new ModalJInternalFrame();
	}
	
	ModalJInternalFrame() {
		// user interface
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("ModalJInternalFrame");
		Container cp = getContentPane();
		JLayeredPane jlp = getLayeredPane();
		
		newPLayer = getGlassPane();
		newPLayer.addMouseListener(new MouseAdapter(){});
		newPLayer.addKeyListener(new KeyAdapter(){});
		jlp.add(newPLayer, JLayeredPane.PALETTE_LAYER);
		
		makeModalJIF();
		jlp.add(modalJIF, JLayeredPane.MODAL_LAYER);
		
		JPanel p = new JPanel();
		p.setLayout(new GridLayout(3, 3));
		for (int i = 0; i < 9; i++) {
			p.add(new JButton(Integer.toString(i)));
		}
		cp.add(p);
		JButton showIntFrame = new JButton("ShowFrame");
		showIntFrame.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				modalJIF.setVisible(true);
				newPLayer.setVisible(true);
			}
		});
		cp.add(showIntFrame, BorderLayout.SOUTH);
		pack();
		setVisible(true);
	}
	
	private void makeModalJIF() {
		JButton hideModalJIF = new JButton("hide me");
		hideModalJIF.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				modalJIF.setVisible(false);
				newPLayer.setVisible(false);
			}
		});
		modalJIF.getContentPane().add(hideModalJIF);
		modalJIF.pack();
	}
}