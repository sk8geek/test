import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.text.DateFormat;
import java.text.ParseException;

public class SpinnerTest extends JFrame {

	private static final DateFormat ITEMDATE = 
    	DateFormat.getDateInstance(DateFormat.MEDIUM);
	private SpinnerDateModel DoBModel = new SpinnerDateModel(
		new Date(), new Date(System.currentTimeMillis() 
		- 94694400000l), new Date(), Calendar.MONTH);
	private JSpinner sp_DoB = new JSpinner(DoBModel);

	private JSpinner.DateEditor sp_mediumDateEditor = 
		new JSpinner.DateEditor(new JSpinner(DoBModel), "dd MMM yyyy");


	public static void main(String[] a){
		SpinnerTest cleanFrame = new SpinnerTest();
	}
	
	SpinnerTest() {
		// user interface
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("SpinnerTest");
		Container cp = getContentPane();
		cp.setLayout(new FlowLayout(FlowLayout.LEFT, 8, 8));

		sp_DoB.setEditor(sp_mediumDateEditor);
		cp.add(sp_DoB);

		JButton update = new JButton("Update");
		update.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evnt) {
				try {
					sp_DoB.commitEdit();
				} catch (ParseException pex) {
					System.out.println("parse error");
				}
				System.out.println("updated");
			}
		});

		JButton report = new JButton("Report");
		report.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evnt) {
				System.out.println("DoBModel=" + DoBModel.getDate());
//				System.out.println("sp_DoB value=" + sp_DoB.getValue());
			}
		});
		
		cp.add(update);
		cp.add(report);
		pack();
		show();
	}
}