import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class TigerUI extends JFrame {

	private TigerNameArray<TigerFullName> tna = new TigerNameArray<TigerFullName>();

	public static void main (String[] args) {
		TigerUI ui = new TigerUI();
	}

	TigerUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Generics Test");
		Container cp = getContentPane();
		cp.setLayout(new FlowLayout());

		final JTextField given = new JTextField(10);
		final JTextField surname = new JTextField(10);
		JButton addName = new JButton("Add");
		addName.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				addNameToArray(given.getText(), surname.getText());
			}
		});
		JButton listMembers = new JButton("List");
		listMembers.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				listNamesFromArray();
			}
		});
		cp.add(given);
		cp.add(surname);
		cp.add(addName);
		cp.add(listMembers);
		pack();
		setVisible(true);
	}

	private void addNameToArray(String given, String family) {
		TigerFullName fullName = new TigerFullName(given, family);
//		tna.addPerson(fullName);
		tna.add(fullName);
		System.out.println("person added to array");
	}

	private void listNamesFromArray() {
		tna.listMembers();
	}

}
