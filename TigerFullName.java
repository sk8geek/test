
public class TigerFullName<TigerFullName> {
	
	private String givenName;
	private String surname;

	TigerFullName() {
		givenName = "";
		surname = "";
	}

	TigerFullName(String given, String family) {
		givenName = given;
		surname = family;
	}
	
	public String getGivenName() {
		return givenName;
	}
	
	public String getSurname() {
		return surname;
	}
}