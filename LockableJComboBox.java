import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class LockableJComboBox extends JFrame {

	public static void main(String[] a){
		LockableJComboBox cleanFrame = new LockableJComboBox();
	}
	
	LockableJComboBox() {
		// user interface
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("LockableJComboBox");
		Container cp = getContentPane();

		JPanel p = new JPanel();
		String[] cBoxItems = new String[] {
			"Washers", "Nuts", "Screws", "Bolts" };

		final JComboBox cBox = new JComboBox(cBoxItems);

		JButton lockCombo = new JButton("Lock");
		lockCombo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cBox.setPopupVisible(false);
				cBox.setEditable(false);
			}
		});
		
		
		p.add(new JButton("nothing"));
		p.add(cBox);
		p.add(new JButton("nought"));
		cp.add(p);
		cp.add(lockCombo, BorderLayout.SOUTH);
		pack();
		setVisible(true);
	}
}