import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class PaintOrValidate extends JFrame {

	private JPanel mainPanel = new JPanel();
	private Container cp;

	public static void main(String[] a){
		PaintOrValidate cleanFrame = new PaintOrValidate();
	}
	
	PaintOrValidate() {
		// user interface
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("PaintOrValidate");
		cp = getContentPane();
		addNewButton();
		cp.add(mainPanel);
		JPanel buttonPanel = new JPanel();
		JButton buttonAddAnother = new JButton("Hit me");
		buttonAddAnother.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evnt){
				addNewButton();
			}
		});
		buttonPanel.add(buttonAddAnother);
		
		JButton buttonValidateF = new JButton("Validate JFrame");
		buttonValidateF.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evnt){
				validate();
			}
		});
		buttonPanel.add(buttonValidateF);
		
		JButton buttonRepaintF = new JButton("Repaint JFrame");
		buttonRepaintF.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evnt){
				repaint();
			}
		});
		buttonPanel.add(buttonRepaintF);

		JButton buttonValidateCP = new JButton("Validate Content Pane");
		buttonValidateCP.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evnt){
				cp.validate();
			}
		});
		buttonPanel.add(buttonValidateCP);
		
		JButton buttonRepaintCP = new JButton("Repaint Content Pane");
		buttonRepaintCP.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evnt){
				cp.repaint();
			}
		});
		buttonPanel.add(buttonRepaintCP);
		
		JButton buttonValidateP = new JButton("Validate JPanel");
		buttonValidateP.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evnt){
				mainPanel.validate();
			}
		});
		buttonPanel.add(buttonValidateP);
		
		JButton buttonRepaintP = new JButton("Repaint JPanel");
		buttonRepaintP.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evnt){
				mainPanel.repaint();
			}
		});
		buttonPanel.add(buttonRepaintP);

		cp.add(buttonPanel, BorderLayout.SOUTH);
		setSize(1000,200);
		setVisible(true);
	}
	
	private void addNewButton() {
		mainPanel.add(new JButton("new"));
	}
}