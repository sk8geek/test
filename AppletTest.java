import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.applet.*;


public class AppletTest extends JApplet {


	public void init() {
		userInterface();
	}

	
	public void start() {
	}


	public void stop() {
	}

	
	public void destroy() {
	}


	private void userInterface () {
		final Container cp = getContentPane();
		JTextArea t = new JTextArea("You can edit this text.",4,20);
		JButton b = new JButton("Open Frame");
		b.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evnt){
				JFrame f = new JFrame("Hello");
				f.getContentPane().add(new JButton("Do nothing"));
				f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				f.pack();
				f.show();
			}
		});
		cp.add(t, BorderLayout.CENTER);
		cp.add(b, BorderLayout.SOUTH);
	}
}