import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;

public class StockCutter2 extends JFrame{

	private JTextField stockLength = new JTextField("10", 4);
	private JTextField pieceLength = new JTextField("", 4);
	private JTextField pieceCount = new JTextField("", 4);
	private int s = 0;
	private int W = 0;
	private ArrayList pieces = new ArrayList();
	private	JButton addPiece = new JButton("Add");
	private	JButton clearPieces = new JButton("Clear");
	private	JButton calculate = new JButton("Calculate");
	private ArrayList workList = new ArrayList();


	public static void main(String args[]) {
		StockCutter2 s = new StockCutter2();
	}

	StockCutter2() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Stock Cutter 2");
		userInterface();
	}
	
	private void addPieces() {
		int numberOf = Integer.parseInt(pieceCount.getText());
		int lengthOf = Integer.parseInt(pieceLength.getText());
		for (int i = 0; i < numberOf; i++) {
			pieces.add(new Integer(lengthOf));
		}
	}
	
	private void calculateCuts() {
		Object[] sortedPieces = pieces.toArray();
		Arrays.sort(sortedPieces);
		/* The array is actually in the reverse of the order that I want
		   as I'd like biggest piece first.  
		   Also, as I want to be able to take 
		   elements out I move the whole lot back to an ArrayList. */
		workList.clear();
		for (int i = sortedPieces.length - 1; i >= 0; i--) {
			workList.add(sortedPieces[i]);
		}
		while (workList.size() > 0) {
			int thisStockLength = Integer.parseInt(stockLength.getText());
			System.out.println("New stock piece taken");
			int cutPiece = 0;
			while ((cutPiece = longestThatFits(thisStockLength)) > 0) {
				thisStockLength -= cutPiece;
				System.out.println(cutPiece + " cut");
			}
			System.out.println("waste from this stock piece=" + thisStockLength);
		}
	}
	
	private int longestThatFits(int remainingLength) {
		for (int i = 0; i < workList.size(); i++) {
			if (((Integer)workList.get(i)).intValue() <= remainingLength) {
				int takePiece = ((Integer)workList.get(i)).intValue();
				workList.remove(i);
				return takePiece;
			}
		}
		return 0;
	}
	
	private void userInterface() {
	
		JPanel p1 = new JPanel();
		p1.add(new JLabel("Stock length"));
		p1.add(stockLength);
		
		JPanel p2 = new JPanel();
		p2.add(clearPieces);
		p2.add(new JLabel("Piece length"));
		p2.add(pieceLength);
		p2.add(new JLabel(" Qty"));
		p2.add(pieceCount);
		p2.add(addPiece);
		
		Container cp = getContentPane();
		cp.add(p1, BorderLayout.NORTH);
		cp.add(p2);
		cp.add(calculate, BorderLayout.SOUTH);
		
		clearPieces.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evnt) {
				pieces.clear();
				System.out.println("pieces cleared");
			}
		});
		
		addPiece.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evnt) {
				setFieldEditing(false);
				addPieces();
				setFieldEditing(true);
			}
		});
		
		calculate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evnt) {
				setFieldEditing(false);
				calculateCuts();
				setFieldEditing(true);
			}
		});
		JPanel buttonPanel = new JPanel();
		buttonPanel.add(calculate);
		getContentPane().add(buttonPanel, BorderLayout.SOUTH);
		
		pack();
		setVisible(true);
	}
	
	private void setFieldEditing(boolean editable) {
		stockLength.setEnabled(editable);
		pieceLength.setEnabled(editable);
		pieceCount.setEnabled(editable);
		addPiece.setEnabled(editable);
		calculate.setEnabled(editable);
	}
}
