import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class ScrollFlowTest2 extends JFrame {

	public static void main(String[] a){
		ScrollFlowTest2 s = new ScrollFlowTest2();
	}
	
	ScrollFlowTest2() {
		// user interface
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("ScrollFlowTest");
		setSize(new Dimension(140,140));

		JPanel p = new JPanel();
//		p.setPreferredSize(new Dimension(100,100));
		
		JButton b_t1 = new JButton("Test1");
		JButton b_t2 = new JButton("Test2");
		JButton b_t3 = new JButton("Test3");
		JButton b_t4 = new JButton("Test4");
		JButton b_t5 = new JButton("Test5");
		JButton b_t6 = new JButton("Test6");
		JButton b_t7 = new JButton("Test7");
		JButton b_t8 = new JButton("Test8");

//		p.setPreferredSize(new Dimension(100,100));

		
		p.add(b_t1);
		p.add(b_t2);
		p.add(b_t3);
		p.add(b_t4);
		p.add(b_t5);
		p.add(b_t6);
		p.add(b_t7);
		p.add(b_t8);

		getContentPane().add(p);
System.out.println(p.getSize());
		p.validate();
System.out.println(p.getSize());


//		JScrollPane sp = new JScrollPane(p);

/*
		JScrollPane sp = new JScrollPane(p, 
			JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
			JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
*/
//		JViewport vp = sp.getViewport();
//		vp.setLayout(new FlowLayout());

		

//		sp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
//		sp.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

//		getContentPane().add(sp);
		
//		JViewport vp = sp.getViewport();
//		vp.setViewSize(new Dimension(150,100));

//		pack();
//		sp.getViewport().setPreferredSize(getSize());
		show();
System.out.println(p);
	}

}

class ScrollableJPanel extends JPanel implements Scrollable{

	public Dimension getPreferredScrollableViewportSize() {
		return null;
	}

	public int getScrollableBlockIncrement(Rectangle vis, int orientation, 
		int direction){
		return 1;
	}
	
	public boolean getScrollableTracksViewportHeight() {
		return false;
	}
	
	public boolean getScrollableTracksViewportWidth() {
		return true;
	}
	
	public int getScrollableUnitIncrement(Rectangle vis, int orientation,
		int direction){
		return 1;
	}
}