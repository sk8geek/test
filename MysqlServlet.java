import java.util.*;
import java.sql.*;
import com.mysql.jdbc.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;



public class MysqlServlet extends HttpServlet { 

   public void doGet(HttpServletRequest req, HttpServletResponse resp) 
      throws ServletException, IOException {
      
      // this handles the initial request, prompt for a badge number
      
      resp.setContentType("text/html");
      PrintWriter out = resp.getWriter();
      
      out.println("<html><head><title>On-line Hardware Inventory</title></head>");
      out.println("<body><p>Please enter the PC badge number</p>");
      out.println("<form method=\"post\" action=\"/servlets/hwinvquery\">");
      out.println("<input type=\"text\" name=\"badgeno\" size=\"5\"><br>");
      out.println("<input type=\"submit\" value=\"Submit Query\"></form></body></html>");

   }
   
   
   public void doPost(HttpServletRequest req, HttpServletResponse resp) 
      throws ServletException, IOException {
      
      resp.setContentType("text/html");
      PrintWriter out = resp.getWriter();

      out.println("<html><head><title>On-line Hardware Inventory, Results</title></head>");
      
      try {
      
         DriverManager.registerDriver(new com.mysql.jdbc.Driver());
         java.sql.Connection con = DriverManager.getConnection("jdbc:mysql://ls-db:3306/itres","dbq","quizmaster");
         
         java.sql.Statement smnt = con.createStatement();
         String sqlquery = new String("SELECT * FROM hw_badged WHERE badge="+req.getParameter("badgeno"));
         java.sql.ResultSet res = smnt.executeQuery( sqlquery );
         res.next();
         
         out.println("<body><p>Results:</p>");
         
         out.println("Badge: " + res.getString("badge") + "<br>");
         out.println("Descr: " + res.getString("descrip") + "<br>");
         out.println("User:  " + res.getString("user") + "<br>");
         
         out.println("</p></body></html>");
         
         res.close();
         smnt.close();
         con.close();
         
      }
      
      catch (SQLException sqlex) {
      
         String msg = new String(sqlex.getMessage());
         
         out.println("<body><p>Something broke...<br>");
         out.println("SQL Exception:" + msg + "</p></body></html>");

      
      }

   }

}


