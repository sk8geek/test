import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class testFullscreen extends JFrame {

	private static Toolkit awTk;

	testFullscreen() {
		Container cp = getContentPane();
		cp.setLayout(new FlowLayout());
		JButton b_big = new JButton("big");
		b_big.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evnt){
				setSize(awTk.getScreenSize());
			}
		});
		JButton b_small = new JButton("small");
		b_small.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evnt){
				setSize(800,600);
			}
		});
		cp.add(b_big);
		cp.add(b_small);
		setSize(800,600);
		show();
	}


	public static void main(String[] a){
    awTk = Toolkit.getDefaultToolkit();
		testFullscreen t = new testFullscreen();
		t.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}