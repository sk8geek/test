import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class SwingTest extends JFrame implements Runnable {

	private Thread goBoy;
	private boolean tfGotFocus = false;
	private JTextField t;
	
	public static void main(String[] a){
		SwingTest s = new SwingTest();
	}
	
	SwingTest() {
		// user interface
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("SwingTest");
		Container cp = getContentPane();
		JPanel p = new JPanel(new FlowLayout());
		
		ImageIcon ii = new ImageIcon("arrow.png");
		final RotatingIconButton b = new RotatingIconButton(ii);
		b.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evnt){
				b.rotate();
			}
		});
		p.add(b);
		
		t = new JTextField("Edit me");
		t.addFocusListener(new FocusListener(){
			public void focusGained(FocusEvent evnt){
				setTfGotFocus(true);
				startThread();
			}
			public void focusLost(FocusEvent evnt){
				setTfGotFocus(false);
			}
		});
		p.add(t);
		
		JTextArea ta = new JTextArea(4,20);
		JScrollPane sp = new JScrollPane(ta,
			JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, 
			JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		p.add(sp);

		cp.add(p);
		pack();
		show();


	}


	private void startThread(){
    goBoy = new Thread(this);
    goBoy.setPriority(Thread.MIN_PRIORITY);
		goBoy.start();
	}

	private void setTfGotFocus(boolean focusState){
		tfGotFocus = focusState;
	}


  public void run() {
    while ( tfGotFocus ) {
    	if (t.getBackground() == Color.blue) {
	    	t.setBackground(Color.green);
	    } else {
	    	t.setBackground(Color.blue);
	    }
      try {
        goBoy.sleep(1000);
      }
      catch (InterruptedException iex) {
        // ignore
      }
    }
  }
}



class RotatingIconButton extends JButton {

	private int pointTo;
	private ImageIcon ii;

	RotatingIconButton(ImageIcon i){
		super(i);
		ii = i;
		pointTo = 0;
	}

	public void rotate(){
		pointTo++;
		if (pointTo>3) { pointTo=0; }
	}

	public void pointTo(int p){
		pointTo = p;
	}
	
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		Graphics2D gg = (Graphics2D)g.create();
		gg.rotate(pointTo*Math.PI/2,7,7);
		gg.drawImage(ii.getImage(),0,0,this);
	}

}