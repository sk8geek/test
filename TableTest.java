import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;
import java.util.*;


public class TableTest extends JFrame {

	public static void main(String[] a) {
		TableTest tt = new TableTest();
		tt.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	TableTest() {
		JDesktopPane jdp = new JDesktopPane();
		Container cp = getContentPane();
		Vector rowA = new Vector(3);
		Vector rowB = new Vector(3);
		Vector rowC = new Vector(3);
		Vector allRows = new Vector(3);
		Vector headings = new Vector(3);
		
		
		headings.add("Qty");
		headings.add("Colour");
		headings.add("Shape");
		
		rowA.add("One");
		rowA.add("Red");
		rowA.add("Circle");

		rowB.add("Two");
		rowB.add("Orange");
		rowB.add("Eye");

		rowC.add("Three");
		rowC.add("Yellow");
		rowC.add("Triangle");
		
		allRows.add(rowA);
		allRows.add(rowB);
		allRows.add(rowC);
		
		JTable t = new JTable(allRows, headings);
		TableColumn tc = new TableColumn();
		tc.setCellEditor(new cbCell());
		tc.setCellRenderer(new cbCell());
		tc.setHeaderValue("Value");
		t.addColumn(tc);
		t.setRowHeight(t.getRowHeight()+10);
		
		JTextField tf = new JTextField(20);
		
		cp.add(new JScrollPane(t));
		cp.add(tf, BorderLayout.SOUTH);
		pack();
		show();
	}
}

class cbCell extends JComboBox implements TableCellEditor, TableCellRenderer {

	private static Vector phon = new Vector(3);

	static {
		phon.add("alpha");
		phon.add("bravo");
		phon.add("charlie");
	}
	
	cbCell() {
		super(phon);
		setEditable(true);
	}

	public Component getTableCellEditorComponent(
		JTable table, Object value, boolean isSelected, int row, int col) {
		setSelectedItem(value);
		return this;
	}

	public Component getTableCellRendererComponent(
		JTable table, Object value, boolean isSelected, boolean hasFocus, 
		int row, int col) {
		setSelectedItem(value);
		return this;
	}

	public void addCellEditorListener(CellEditorListener cel){
	}

	public void removeCellEditorListener(CellEditorListener cel){
	}

	public boolean shouldSelectCell(EventObject o){
		return true;
	}
	
	public boolean isCellEditable(EventObject o){
		return true;
	}
	
	public Object getCellEditorValue() {
		return this.getSelectedItem();
	}
	
	public boolean stopCellEditing() {
		return true;
	}

	public void cancelCellEditing() {
	}

}




