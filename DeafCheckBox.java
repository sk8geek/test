import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;
import javax.swing.plaf.basic.BasicButtonListener;
import java.util.*;
import java.beans.*;


public class DeafCheckBox extends JFrame {

	public static void main(String[] a) {
		DeafCheckBox dt = new DeafCheckBox();
		dt.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	DeafCheckBox() {
		Container cp = getContentPane();

		JButton btn = new JButton("nothing");
		cp.add(btn, BorderLayout.NORTH);
		
		JCheckBox cbx = new JCheckBox("Can't click");
		
		cp.add(cbx);

		EventListener[] ears = (EventListener[])(
			cbx.getListeners(ChangeListener.class));
//		cbx.removeChangeListener((ChangeListener)ears[0]);
//		cbx.removePropertyChangeListener((PropertyChangeListener)ears[0]);
//		cbx.removeFocusListener((FocusListener)ears[0]);
		cbx.removeMouseListener((MouseListener)ears[0]);
//		cbx.removeMouseMotionListener((MouseMotionListener)ears[0]);
//		EventListener[] ears2 = (EventListener[])(
//			cbx.getListeners(FocusListener.class));
//		cbx.removeFocusListener((FocusListener)ears2[0]);
		cbx.setFocusable(false);


System.out.println("ActionListeners");
		ActionListener[] al = cbx.getActionListeners();
		for (int i=0; i < al.length; i++ ) {
			System.out.println(al[i]);
		}

System.out.println("ChangeListeners");
		ChangeListener[] cl = cbx.getChangeListeners();
		for (int i=0; i < cl.length; i++ ) {
			System.out.println(cl[i]);
		}

System.out.println("ItemListeners");
		ItemListener[] il = cbx.getItemListeners();
		for (int i=0; i < il.length; i++ ) {
			System.out.println(il[i]);
		}

System.out.println("AncestorListeners");
		AncestorListener[] anl = cbx.getAncestorListeners();
		for (int i=0; i < anl.length; i++ ) {
			System.out.println(anl[i]);
		}

System.out.println("PropertyChangeListeners");
		PropertyChangeListener[] pl = cbx.getPropertyChangeListeners();
		for (int i=0; i < pl.length; i++ ) {
			System.out.println(pl[i]);
		}

System.out.println("VetoableChangeListeners");
		VetoableChangeListener[] vl = cbx.getVetoableChangeListeners();
		for (int i=0; i < vl.length; i++ ) {
			System.out.println(vl[i]);
		}

System.out.println("ContainerListeners");
		ContainerListener[] col = cbx.getContainerListeners();
		for (int i=0; i < col.length; i++ ) {
			System.out.println(col[i]);
		}

System.out.println("ComponentListeners");
		ComponentListener[] cmpl = cbx.getComponentListeners();
		for (int i=0; i < cmpl.length; i++ ) {
			System.out.println(cmpl[i]);
		}

System.out.println("FocusListeners");
		FocusListener[] fl = cbx.getFocusListeners();
		for (int i=0; i < fl.length; i++ ) {
			System.out.println(fl[i]);
		}

System.out.println("HierarchyBoundsListeners");
		HierarchyBoundsListener[] hl = cbx.getHierarchyBoundsListeners();
		for (int i=0; i < hl.length; i++ ) {
			System.out.println(hl[i]);
		}

System.out.println("InputMethodListeners");
		InputMethodListener[] iml = cbx.getInputMethodListeners();
		for (int i=0; i < iml.length; i++ ) {
			System.out.println(iml[i]);
		}

System.out.println("KeyListeners");
		KeyListener[] kl = cbx.getKeyListeners();
		for (int i=0; i < kl.length; i++ ) {
			System.out.println(kl[i]);
		}

System.out.println("MouseListeners");
		MouseListener[] ml = cbx.getMouseListeners();
		for (int i=0; i < ml.length; i++ ) {
			System.out.println(ml[i]);
		}

System.out.println("MouseMotionListeners");
		MouseMotionListener[] mml = cbx.getMouseMotionListeners();
		for (int i=0; i < mml.length; i++ ) {
			System.out.println(mml[i]);
		}

System.out.println("MouseWheelListeners");
		MouseWheelListener[] mwl = cbx.getMouseWheelListeners();
		for (int i=0; i < mwl.length; i++ ) {
			System.out.println(mwl[i]);
		}


		cp.add(new JButton("click"), BorderLayout.SOUTH);
		pack();
		show();
	}


	private void deafenComponent(Component c) {
System.out.println(c);
		MouseListener[] existingEars = c.getMouseListeners();
		for (int i=0; i < existingEars.length; i++ ) {
			c.removeMouseListener(existingEars[i]);
		}
		MouseMotionListener[] existingMotionEars = c.getMouseMotionListeners();
		for (int i=0; i < existingMotionEars.length; i++ ) {
			c.removeMouseMotionListener(existingMotionEars[i]);
		}
		MouseWheelListener[] mouseWheelEars = c.getMouseWheelListeners();
		for (int i=0; i < mouseWheelEars.length; i++ ) {
			c.removeMouseWheelListener(mouseWheelEars[i]);
		}
	}
}