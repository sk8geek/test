import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class SafeClosingFrame extends JFrame {

	private JFrame thisFrame;

	public static void main(String[] a){
		SafeClosingFrame s = new SafeClosingFrame();
	}
	
	SafeClosingFrame() {
		thisFrame = this;
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setTitle("SaveClosingFrame");
		
		JButton immediateCloseButton = new JButton("Close NOW!");
		immediateCloseButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evnt){
				closeFrame();
			}
		});
		
		addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent evnt){ 
				if (JOptionPane.showConfirmDialog(thisFrame,"Do you really want to quit!?") == 0) {
					closeFrame();
				}
			}
		});
		
		Container cp = getContentPane();
		cp.add(new JTextArea(4,14));
		cp.add(immediateCloseButton, BorderLayout.SOUTH);

		pack();
		show();
	}
	
	private void closeFrame(){
		dispose();
		System.exit(0);
	}
}