import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class MenuDemo extends JFrame {

	private JFrame thisFrame;

	private JMenu m_starters;
	private JMenu m_main;
	private JMenu m_dessert;

	private JTextField tf_starter;
	private JTextField tf_main;
	private JTextField tf_dessert;

	public static void main(String[] a){
		MenuDemo m = new MenuDemo();
	}
	
	MenuDemo() {
		thisFrame = this;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("MenuDemo");
		userInterface();
		m_main.setEnabled(false);
		m_dessert.setEnabled(false);
		show();
	}
		


	private void userInterface(){

		JMenuBar mb = new JMenuBar();

		m_starters = new JMenu("Starters");
		JMenuItem mi_soup = new JMenuItem("Soup");
		JMenuItem mi_salad = new JMenuItem("Salad");
		JMenuItem mi_nachos = new JMenuItem("Nachos");
		m_starters.add(mi_soup);
		m_starters.add(mi_salad);
		m_starters.add(mi_nachos);
		
		m_main = new JMenu("Main");
		JMenuItem mi_fish = new JMenuItem("Fish");
		JMenuItem mi_pizza = new JMenuItem("Pizza");
		JMenuItem mi_burger = new JMenuItem("Burger");
		m_main.add(mi_fish);
		m_main.add(mi_pizza);
		m_main.add(mi_burger);

		m_dessert = new JMenu("Dessert");
		JMenuItem mi_icecream = new JMenuItem("Ice Cream");
		JMenuItem mi_cheese = new JMenuItem("Cheese");
		JMenuItem mi_cake = new JMenuItem("Cake");
		m_dessert.add(mi_icecream);
		m_dessert.add(mi_cheese);
		m_dessert.add(mi_cake);

		JTextField tf_starter = new JTextField("pick one");
		tf_starter.setEditable(false);

		JTextField tf_main = new JTextField("pick one");
		tf_main.setEditable(false);

		JTextField tf_dessert = new JTextField("pick one");
		tf_dessert.setEditable(false);
		
		mb.add(m_starters);
		mb.add(m_main);
		mb.add(m_dessert);
		
		this.setJMenuBar(mb);
		
		JPanel p = new JPanel();
		p.add(tf_starter);
		p.add(tf_main);
		p.add(tf_dessert);
		
		getContentPane().add(p);
		
		pack();
	}
	
}